'use strict';
module.exports = function (grunt) {
  grunt.initConfig({
    sass : {
      dist: {
        files : {
          'public/css/style.css': 'sass/style.scss',
          'public/css/hover.css': 'sass/hover.scss'
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-sass');
  grunt.registerTask('default', ['sass']);
};