var http = require("http")
    , querystring = require('querystring')
    , keys = require("../keys_file.js")
    , host = "http://www.behance.net/v2/"
    , apiKey = keys.behanceConsumerKey
    , secret = keys.behanceClientSecret
    , $ = require("jquery");

function Behance () {
  this.apiKey = apiKey;
  this.secret = secret;
  this.host = host;
  return this;
}    

Behance.prototype = {
  _request: function(path, query, cb) {
    
    var that = this;
    var options = {
      host: that.host,
      port: 80,
      path: path + "?" + query.toString(),
      method: 'GET'
    };
    console.log(options);
    var req = http.request(options, function(res) {
      var json = "";
      res.on('data', function(junk) {
        json += junk;
      });
      res.on('end', function() {
        cb(JSON.parse(json));
      });
      res.on('error', function(e) {
        cb(undefined);
      });
    });
    req.on('error', function(e) {
      console.log(e);
      cb(undefined);
    });
    req.end();
  },

  _getUser: function(path, query, cb) {
    var url = this.host + path + "?" + query.toString();
    console.log(url);
    $.get(url, function(data) {
      if(!!data) {
        cb(data);
      }
      else {
        cb(undefined);
      }
    })
    .done(function(data) { console.log( data ); })
    .fail(function(e) { console.log( e ); })
    .always(function() { console.log( "complete" ); });;
    },

  users: function(name) {
    if(!name) {
      throw new Error("You must specify a user name!!");
    }
    var username = name,
        that = this;
    return {
      details: function (cb) {
        return that._getUser("users/"+username, "api_key="+that.apiKey, cb);
      },
      projects: function (cb) {
        return that._getUser("users/"+username+"/projects", "api_key="+that.apiKey, cb);
      },
      wips: function (cb) {
        return that._getUser("users/"+username+"/wips", "api_key="+that.apiKey, cb);
      }
    }
  }
};

var behance = new Behance();

exports.details = function(req, res) {
  try {
    behance.users("matiascorea").details(function(data) {
      console.log(data);
      //res.render('behance', { feed : data });
      res.send(data);
    });
  }
  catch (e) {
    res.send(e.message);
  } 
};

exports.list = function(req, res) {
  var user = behance.users("matiascorea"),
      list = req.params.list;

  try {
    user[list](function(data) {
      res.send(data);
    });
  }
  catch (e) {
    res.send(e.message);
  }    
}