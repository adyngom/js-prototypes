
/*
 * GET home page.
 */
var fs = require("fs")
    , walk = require("walk")
    , files = []
    , walker =  walk.walk('./public/images', {followlinks : false});

walker.on('file', function(root, stat, next){
  if (stat.name.charAt(0) !== "." ) {
    files.push('images/' +stat.name);
  }  
  next();
});

walker.on('end', function() {
  console.log(files);
});

exports.index = function(req, res){
  res.render('offline', { title: "App is offline", folio: files});
};

exports.entry = function(req, res){
  res.render('entry', { meta: req.params });
};