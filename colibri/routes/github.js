var GitHubApi = require("github");

var github = new GitHubApi({
    // required
    version: "3.0.0",
    // optional
    timeout: 5000
});

exports.index = function(request, response) {
  github.user.get({
      user: "addyosmani"
  }, function(err, res) {
    if(err) {
      console.log(err);
      return;
    }
      response.send(JSON.stringify(res));
  });
};  