/**
 * Module dependencies.
 */
var express = require('express')
  , hbs = require("express3-handlebars").create({
      defaultLayout: "main",
      partialsDir: "views/layouts/partials/"
    })
  , routes = require('./routes')
  , subdomain = require("subdomain")
  , user = require('./routes/user')
  , dribbble = require('./routes/dribbble')
  , behance = require('./routes/behance')
  , github = require('./routes/github')
  , stackEx = require('./routes/stackExchange')
  , offline  = require('./routes/offline')
  , http = require('http')
  , path = require('path')
  //, github = require('github')
  , parser = require('parse-rss')
  , sass = require("node-sass");

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
//app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(
  sass.middleware({
       src: __dirname + '/sass', //where the sass files are 
       dest: __dirname + '/public', //where css should go
       debug: true // obvious
   })
);
//app.use(require('express-subdomain-handler')({ baseUrl: 'localhost:3000/sub', prefix: 'myprefix', logger: true }) );
app.use(app.router);
//app.use(subdomain({ base : 'myfolio.dev', removeWWW : true }));
// app.use(require('stylus').middleware(__dirname + '/public'));
// app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname+"/public"));
app.engine("handlebars", hbs.engine);
// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.set("view engine", "handlebars");

// app.get('/myprefix/:thesubdomain/thepage', function(req, res, next){
// 	// for the example url this will print 'mysubdomain'
//     res.send(req.params.thesubdomain);
// }); 

//app.get('/', routes.index);

app.get("/", function(request, response, next) {
  response.setHeader("Content-Type", "text/html");
  //response.send(movies);
  response.render("home");
});

app.get('/offline', offline.index);
app.get('/dribbble', dribbble.shots);
app.get('/stackEx', stackEx.index);
app.get('/behance', behance.details);
app.get('/behance/:list', behance.list);
app.get('/github', github.index);
app.get('/folio/:path/:item', offline.entry);
app.get('/users', user.list);
// app.get('/subdomain/blog', function(req, res) {
// 	res.send('<p>blog.mydomain.com</p>');
// });


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
