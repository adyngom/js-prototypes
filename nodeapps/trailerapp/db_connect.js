var mongo     = require("mongodb")
  , MongoClient = mongo.MongoClient
  , mongoport = mongo.Connection.DEFAULT_PORT
  , db        = new mongo.Db("trailers", new mongo.Server(host, mongoport), {safe:false});

  var TPD = function () {
    return this;
  };

  TPD.prototype = {

    /**
     * Adds a new record to the specified collection
     * @param {Object} col     the object to be inserted
     * @param {String} coltype the name of the collection
     */
    addCollection : function(col, coltype) {
      db.open(function(error) {
        var collection = db.collection(coltype);
        for (var m in col) {
          collection.insert(col, function(err, docs) {
            if(err) {
              console.log(err);
            }
          });
        }

      collection.count(function(err, count) {
          //console.log(format("count = %s", count));
      });
        // Locate all the entries using find
      collection.find().toArray(function(err, results) {
          //console.dir(results);
          // Let's close the db
          db.close();
        });
      });
    },

    /**
     * Finds a single record in the specified collection
     * @param  {String}   table    the name of the collection set
     * @param  {Object}   unique   the query object
     * @param  {Function} callback [description]
     * @return {[type]}            [description]
     */
    findEntry : function (table, unique, callback) {
      //console.log(arguments);
      MongoClient.connect("mongodb://"+host+":"+mongoport+"/"+table, function(err, db) {
        if(err) {
          console.log(err);
          db.close();
          throw(err);
        }

        var collection = db.collection("movies");
        console.log(unique);
        collection.findOne(unique, function(err, doc) {
          if(err) {
            console.log(err);
            throw(err);
          }
          console.log(doc);
          var data = (!!doc ) ? doc : undefined;
          if (!data["yt-trailer"]) {
            youtube.search(data.title+" movie - Official Trailer (HD)", 2, function(ydata) {
              data["yt-trailer"] = ydata;
              callback(data);
            });
          }
          else {
            callback(data);
          }
          
          db.close();
        });
      });
    }
  };

  module.exports.TPD = TPD;