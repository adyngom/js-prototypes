var express = require("express"),
		hbs = require("express3-handlebars").create({
			defaultLayout: "main",
			partialsDir: "views/layouts/partials/",
			helpers: {
				if_gt: function(context, options) {
	        if (context > options.hash.compare)
	            return options.fn(this);
	        return options.inverse(this);
	    	},
	    	link: function(object) {
	  			return new Handlebars.SafeString(
	    			"<a href='" + object.url + "'>" + object.text + "</a>"
	  			);
	  		},
	  		dashed: function(phrase) {
	  			var nospec = phrase.replace(/[^a-zA-Z ]/g, "");
	  			return nospec.replace(/\s+/g, '-').toLowerCase();
	  		}
			}
		}),
		fs 		  	= require("fs"),
		keys      = JSON.parse(fs.readFileSync("config/keys.json")),
		config  	= JSON.parse(fs.readFileSync("config/config.json")),
		nav  	= JSON.parse(fs.readFileSync("config/nav.json")),
		rtc     	= keys.rotten.key,
		tomatoes 	= require("tomatoes"),
		movies  	= tomatoes(rtc.key),
		_lmovies  = JSON.parse(fs.readFileSync("json/opening.json")),
		topTen,
		//movies    = JSON.parse(fs.readFileSync("json/box_office.json")),
		movie     = JSON.parse(fs.readFileSync("json/movie.json"));
		host    	= config.host,
		port    	= config.port,
		//key     	= rtc.key,
		$ = require("jquery"),
		tpd = require("./tp_modules/db_connect.js"),
		format = require("util").format,
		sass      = require("node-sass"),
		youtube   = require("youtube-node"),
		LocalStorage = require("node-localstorage").LocalStorage,
		localstorage = new LocalStorage('./movies'),
		// routes
	app 	  	= express();
	app.use(
		sass.middleware({
         src: __dirname + '/sass', //where the sass files are 
         dest: __dirname + '/public', //where css should go
         debug: true // obvious
     })
	);
function getMovies(list) {
	//console.log(list);
	movies.getList("movies", list, function(err,data) {
		if(!list) {
			return;
		}
		if(err) {
			console.log("Something went wrong: ", err);
			return false;
		}
		else {
			fs.writeFile("json/"+list+".json", JSON.stringify(data,null,2), function(err) {
				if(err) {
					console.log(err);
					throw new Error(err.message);
				}
				data = data.movies;
			});
		}
	});
}

function getMovieRecord(id) {
	var record;
	// if (!!localstorage.getItem(id)) {
	// 	console.log("from storage!!");
	// 	record = JSON.parse(localstorage.getItem(id));
	// }
	// else {
		var that = tpd.findEntry("trailers", {"id" : id.toString()}, function(data) {
			record = data;
		});
		//record = movie;
		//localstorage.setItem(id, JSON.stringify(movie));
		console.log("new record!!");
	//}
	//return record;
}
//addCollection(movies);
console.log(tpd.tpd());
app.use(app.router);
app.use(express.static(__dirname+"/public"));
app.engine("handlebars", hbs.engine);
app.set("view engine", "handlebars");

app.get("/", function(request, response, next) {
	response.setHeader("Content-Type", "text/html");
	var movieset = getMovies("opening");
	console.log(movieset);
	//var latest = JSON.parse(fs.readFileSync("json/opening.json"));
	response.render("home", { m: _lmovies,  layout: 'main' });
});

app.get("/movie/:id", function(request, response) {
 //var record = getMovieRecord(request.params.id); 	
 tpd.tpd.findEntry("trailers", {"id" : request.params.id}, function(data) {
	if(!!data) {
		console.log(nav);
		response.render("page", { m : data, layout: 'entry'});
	}
	else {
		response.send("We could not find a record of that movie");
	}
 });	
});

app.listen(port, host);