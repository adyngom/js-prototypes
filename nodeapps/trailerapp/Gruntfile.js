'use strict';
module.exports = function (grunt) {
  grunt.initConfig({
    sass : {
      dist: {
        files : {
          'public/css/styles.css': 'sass/styles.scss',
          'public/css/test.css': 'sass/test.scss',
          'public/css/hover.css': 'sass/hover.scss'
        }
      }
    }
  });
  // grunt.initConfig({
  //   sass: {
  //     compile: {
  //       files: {
  //         //'test/tmp/test.css': 'test/fixtures/test.scss'
  //         'public/css/test.css': 'sass/test.scss'
  //       }
  //     },
  //     include: {
  //       options: {
  //         //'includePaths': ['./test/fixtures/']
  //         'includePaths': ['./public/css']
  //       },
  //       files: {
  //         //'test/tmp/test3.css': 'test/fixtures/includePaths.scss'
  //         'test/tmp/test3.css': 'test/fixtures/includePaths.scss'
  //       }
  //     }
  //   },
  //   nodeunit: {
  //     tasks: ['test/*_test.js']
  //   },
  //   clean: {
  //     test: ['test/tmp']
  //   }
  // });

  // grunt.loadTasks('tasks');
  // grunt.loadNpmTasks('grunt-contrib-clean');
  // grunt.loadNpmTasks('grunt-contrib-nodeunit');

  // grunt.registerTask('default', ['clean', 'sass', 'nodeunit', 'clean']);
  // 
  grunt.loadNpmTasks('grunt-sass');
  grunt.registerTask('default', ['sass']);
};