var Directional = (function (window, undefined) {

  "use strict";

  var _d = window.document;
  function Directional(el) {
    console.log(this);
    this._el = _d.querySelectorAll(el);
    if(!this._el) {
      return;
    }
    this.bindEvents();
    return this;
  }

  Directional.prototype = {
    bindEvents: function() {
      var that = this;
      [].slice.call(this._el).forEach( function(item){
        item.addEventListener("mouseover", function(ev) {
          that.addClass(ev, this, 'in');
        });

        item.addEventListener('mouseout', function (ev) {
          that.addClass( ev, this, 'out' );
        }, false);
      });
    },
    getDirection : function (ev, obj) {
      var w = obj.offsetWidth,
          h = obj.offsetHeight,
          x = (ev.pageX - obj.offsetLeft - (w / 2) * (w > h ? (h / w) : 1)),
          y = (ev.pageY - obj.offsetTop - (h / 2) * (h > w ? (w / h) : 1)),
          d = Math.round( Math.atan2(y, x) / 1.57079633 + 5 ) % 4;   
      return d;
    },
    addClass : function ( ev, obj, state ) {
      var direction = this.getDirection( ev, obj ),
          class_suffix = "";
      
      obj.className = "";
      
      switch ( direction ) {
          case 0 : class_suffix = '-top';    break;
          case 1 : class_suffix = '-right';  break;
          case 2 : class_suffix = '-bottom'; break;
          case 3 : class_suffix = '-left';   break;
      }
      
      obj.classList.add( state + class_suffix );
    }
  };

  return Directional;

}(window, undefined));