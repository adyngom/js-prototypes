var simple = require('simple-blog');

// ALL PARAMETERS ARE OPTIONAL
simple.setup({
  name: 'Test Blog', // Blog Title

  // How to generate slugs (defaults to "title")
  slugType: 'base36', // options: "title" | "base36"

  // Path to the root of your app
  rootDir: __dirname,

  // Specify a URL that will dump all data
  //   Leave out or set to FALSE to disable
  dumpPath: '/dump',

  // Set amount of posts per page (defaults to 3)
  pageSize: 5,

  // Path to Express.js directories
  publicPath: '/public',
  viewPath: '/views',

  // Redirect www.domain... to domain...
  redirectWWW: true,

  /*************************************************
   * The rest is used for the RSS feed
   */

  // Disable RSS
  // rss: false,

  // Enable RSS at /rss.xml (defaults to disabled)
  rss: {
    description: 'A test blog to showcase simple-blog',
    author: 'Gregory Schier',
    img: '/favicon.ico', // Feed image
    limit: 10 // Feed item cutoff
  }
});

// Add custom routes the same as you would with Express.js
simple.app.get('/about', function(req, res) {
  res.render('about', { title: 'About' });
});

// Start Simple Blog
simple.start();