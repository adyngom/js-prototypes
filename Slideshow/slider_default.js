;(function($){
	var useGPU = Modernizr.csstransforms3d,
		useCSS3 = Modernizr.csstransforms && Modernizr.csstransitions,
		noneTransition = function(container, curIndex, nextIndex){
			$(this.elements[curIndex]).css({display : "none"});
			
			$(this.elements[nextIndex]).css({display : "block"});
		},
		newsFlashTransition = function(container, curIndex, nextIndex){
			$(this.elements[nextIndex]).css($.extend(this._vendor(
				"transform", "scale(1, 1) rotate(0deg)"
			), {
				"z-index" : 2
			}));
			
			$(this.elements[curIndex]).css($.extend(this._vendor(
				"transform", "scale(0, 0) rotate(-360deg)"
			), {
				"z-index" : 1
			}));
		},
		fadeTransition;
	
	if(useCSS3){
		fadeTransition = function(container, curIndex, nextIndex){
			$(this.elements[curIndex]).css({opacity : 0});
			
			$(this.elements[nextIndex]).css({opacity : 1});
		};
	}else{
		fadeTransition = function(container, curIndex, nextIndex){
			$(this.elements[curIndex]).stop().animate({opacity : 0}, this.speed, this.func);
			
			$(this.elements[nextIndex]).stop().animate({opacity : 1}, this.speed, this.func);
		};
	}
	
	
	
	var transitions = {
		none : {
			init : function(container){
				this.elements = $(container).find("> .slide").get();
				
				$().pushStack(this.elements).css({
					display : "none"
				}).first().css({
					display : "block"
				});
			},
			next : noneTransition,
			prev : noneTransition,
			toIndex : noneTransition
		},
		
		fade : {
			init : function(container){
				var self = this,
					$container = $(container);
				
				this.elements = $container.find("> .slide").get();
				
				try{
					if(useGPU){
						$container.css(this._vendor("transform", "translateZ(0)"));
					}
					
					var $elStack = $().pushStack(this.elements);
					
					$elStack.css({
						opacity : 0,
						position : "absolute",
						left : 0,
						top : 0
					}).first().css({
						opacity : 1
					}).parent().css({
						position : "relative"
					});
					
					setTimeout(function(){
						$elStack.css(self._vendor("transition", "opacity " + self.speed + "ms " + self.func));
					}, 0);
				}catch(e){
					throw new Error("init: Container element is not set or contains no inner slides.");
				}
			},
			next : fadeTransition,
			prev : fadeTransition,
			toIndex : fadeTransition
		},
		
		longSlide : {
			init : function(container){
				var $container = $(container);
				
				this.elements = $container.find("> .slide").get();
				
				this.__width = $container.width();
				
				try{
					var $elStack = $().pushStack(this.elements),
						$elContainer = $(document.createElement('div')),
						fullWidth = this.elements.length * this.__width;
					
					$container.css({
						position: "relative"
					});
					
					if(useGPU){
						$container.css(this._vendor(
							"transform", "translateZ(0)"
						));
					}
					
					this.__slideContainer = $elContainer.get();
					
					this.__fullWidth = fullWidth;
					
					$container.css({
						overflow : "hidden"
					}).append($elContainer);
					
					$elStack.css({
						display : "inline-block",
						width : this.__width
					}).appendTo($elContainer);
					
					$elContainer.css($.extend(this._vendor(
						"transition", "left " + this.speed + "ms " + this.func
					), this._vendor("transform", "translateX(0px)"), {
						position : "absolute",
						top : "0",
						left : "0",
						width : fullWidth
					}));
				}catch(e){
					throw new Error("init: Container element is not set or contains no inner slides.");
				}
			},
			next : function(container, curIndex, nextIndex){
				var self = this,
					$container = $(container),
					$sc = $(this.__slideContainer),
					$newSc = $sc.clone();
				
				if(nextIndex === 0){
					$newSc.css({
						left : this.__width
					}).appendTo($container);
					
					setTimeout(function(){
						$sc.css({
							left : -self.__fullWidth
						});
						
						$newSc.css({
							left : 0
						});
					}, 0);
					
					self.__slideContainer = $newSc.get();
				}else{
					$sc.css({
						left : -(nextIndex * this.__width) + "px"
					});
				}
			},
			prev : function(container, curIndex, nextIndex){
				if(nextIndex === this.elements.length - 1){
					
				}else{
					$(this.__slideContainer).css({
						left : -(nextIndex * this.__width) + "px"
					});
				}
			},
			toIndex : function(container, curIndex, nextIndex){
				$(this.__slideContainer).css(this._vendor(
					"transform", "translateX(-" + (nextIndex * this.__width) + "px)"
				));
			}
		},
		
		shortSlide : {
			init : function(container){
				
			},
			next : function(container, curIndex, nextIndex){
				
			},
			prev : function(container, curIndex, nextIndex){
				
			},
			toIndex : function(container, curIndex, nextIndex){
				
			}
		},
		
		newsFlash : {
			init : function(container){
				var self = this,
					$container = $(container);
				
				this.elements = $container.find("> .slide").get();
				
				try{
					if(useGPU){
						$container.css(this._vendor("transform", "translateZ(0)"));
					}
					
					var $elStack = $().pushStack(this.elements);
					
					$elStack.css($.extend(this._vendor(
						"transform", "scale(0, 0) rotate(-360deg)"
					), {
						position : "absolute",
						left : 0,
						top : 0,
						"z-index" : 1,
					})).first().css($.extend(this._vendor(
						"transform", "scale(1, 1) rotate(0deg)"
					), {
						"z-index" : 2
					})).parent().css({
						position : "relative"
					});
					
					setTimeout(function(){
						$elStack.css(self._vendor("transition", "{{vendor}}transform " + self.speed + "ms " + self.func));
					}, 0);
				}catch(e){
					throw new Error("init: Container element is not set or contains no inner slides.");
				}
			},
			next : newsFlashTransition,
			prev : newsFlashTransition,
			toIndex : newsFlashTransition
		}
	};
	
	
	
	for(var i in transitions){
		Slideshow.prototype.addAnimation(i, transitions[i]);
	}
})(jQuery);
