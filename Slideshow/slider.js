;(function(window, $){
	"use strict";
	
	/**
	 * [ description]
	 * @param  {[type]} opts
	 * @return {[type]}
	 */
	var Slideshow = function(opts){
		var self = this;
		
		if(opts.container){
			this._containerElement = opts.container;
		}
		
		if(opts.speed){
			this.speed = opts.speed;
		}
		
		if(opts.func){
			this.func = opts.func;
		}
		
		this.setAnimation("longSlide");
	};
	
	/**
	 * [prototype description]
	 * @type {Object}
	 */
	Slideshow.prototype = {
		_containerElement : undefined,
		_prevElement : undefined,
		_nextElement : undefined,
		_selectors : {
			nestedSlide : undefined,
			left : undefined,
			right : undefined
		},
		index : 0,
		speed : 1500,
		func : "ease-out",
		elements : [],
		_animations : {},
		_animation : undefined,
		_anim : undefined,
		
		_vendor : function(directive, data){
			var ret = {};
			
			ret["-webkit-" + directive] = data.replace(/\{\{vendor\}\}/gi, "-webkit-");
			ret["-moz-" + directive] = data.replace(/\{\{vendor\}\}/gi, "-moz-");
			ret["-o-" + directive] = data.replace(/\{\{vendor\}\}/gi, "-o-");
			ret["-ms-" + directive] = data.replace(/\{\{vendor\}\}/gi, "-ms-");
			ret[directive] = data.replace(/\{\{vendor\}\}/gi, "");
			
			return ret;
		},
		
		
		addAnimation : function(name, anim){
			if(anim.init && anim.next && anim.prev && anim.toIndex){
				this._animations[name] = anim;
			}else{
				throw new Error("Animations must have 'init', 'next', 'prev', and 'toIndex' methods.");
			}
		},
		
		setAnimation : function(anim){
			if(anim in this._animations){
				this._animation = anim;
				
				this._anim = this._animations[anim];
				
				this._anim.init.call(this, this._containerElement);
			}else{
				throw new Error("Animation does not exist.");
			}
		},
		
		prev : function(){
			var cur = this.index;
			
			if(this.index > 0){
				this.index--;
			}else{
				this.index = this.elements.length - 1;
			}
			
			this._anim.prev.call(this, this._containerElement, cur, this.index);
		},
		
		next : function(){
			var cur = this.index;
			
			if(this.index < (this.elements.length - 1)){
				this.index++;
			}else{
				this.index = 0;
			}
			
			this._anim.next.call(this, this._containerElement, cur, this.index);
		},
		
		toIndex : function(newIndex){
			var cur = this.index;
			
			newIndex = parseInt(newIndex);
			
			if(newIndex === this.index){
				return;
			}
			
			if(newIndex >= (this.elements.length)){
				this.index = this.elements.length - 1;
			}else if(newIndex < 0){
				this.index = 0;
			}else{
				this.index = newIndex;
			}
			
			this._anim.toIndex.call(this, this._containerElement, cur, this.index);
		},
		
		call : function(method){
			this._anim[method].call(this, [].slice.call(arguments, 1));
		}
	};
	
	window.Slideshow = Slideshow;
})(window, jQuery);
