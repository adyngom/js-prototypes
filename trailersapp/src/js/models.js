(function (Backbone, TrailerApp, $) { 
	TrailerApp.Models.Trailer = Backbone.Model.extend({
		defaults: {
			cid: "c0"
		},
		
		initialize: function () {
			log("model initialized");
			this.on('change', function(){
			    console.log('values for this model have changed');
			});
		}
	});
	
	TrailerApp.Collections.Trailers = Backbone.Collection.extend({
				model: TrailerApp.Models.Trailer
	});
	
	
	
})(Backbone, TrailerApp, jQuery);	