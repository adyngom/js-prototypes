Backbone.View =  Backbone.View.extend({
	remove: function() {
		// Empty the element and remove it from the DOM while preserving events
		$(this.el).empty().detach();
		return this;
	}
});

TrailerApp.Views.ContentView = Backbone.View.extend({
	/*
	 * Initialize with the template-id
	 */
	initialize: function(view) {
		this.view = view;
	},

	/*
	 * Get the template content and render it into a new div-element
	 */
	/*render: function() {
		var template = $(this.view).html();
		$(this.el).html(template);

		return this;
	}*/
});

TrailerApp.Views.Index = Backbone.View.extend({
	//el         : $("#list"),
	
	//template: _.template($("#movies-list").html()),
	
	initialize : function () {
		this.setElement($("#list")[0]);
		this.render();
	},
	
	events     : {
		"click h1" : "toMoviePage"
	},
	
	show : function () {
		log(this);
	},
	
	toMoviePage : function (event) {
		log($(event.target).data("cid"));
		TrailerApp.router.navigate("movie/" + $(event.target).data("cid").split("").slice(1), true);
	},
	
	render     : function () {
		var that = this;
		var template = _.template( $("#movies-list").html());
		log("render");
		log(that);
		
		$(that.el).empty().append(template({
			movies: that.collection.models
		}));
		//$(this.el).html("<h1>Box office</h1>");
		
		that.$el.appendTo("body");
		return this;
	}
});

TrailerApp.Views.Movie = Backbone.View.extend({
	initialize : function () {
		this.setElement($("#list")[0]);
		this.vidPlayer = $("#offTrail");
		this.vidFrame  = this.vidPlayer.find("iframe");
		this.vidEmbedUrl = "http://www.youtube.com/embed/";
		this.render();
	},
	
	render : function () {
		var that = this;
		var thisMovie = {
			movie: this.model.get('attributes')
		};
		//thisMovie.movie = {this.model.get('attributes')},
		log(thisMovie.movie.title);
		log($.jTrailer(thisMovie.movie.title+" - official trailer", 4, this.parseTrailers));
		var template = _.template( $("#movie-entry").html());
		
		$(that.el).empty().append(template({
			movie: thisMovie
		}));
		
		that.$el.appendTo("body");
		return this;	
	},
	
	parseTrailers: function (data) {
		
		var that = this;
		
		log("[parseTrailers]")
		log(that);
		
		if (!data) {
			alert("No trailer found for this movie");
			return false;
		}
		//log(data);
		var trailers = data.entry,
			medias   = trailers[0]["media$group"],
			trailerId = trailers[0]["id"]["$t"].split("/").pop();
			log(trailerId);
			log(medias["media$thumbnail"]);
			
		
		var vidPlayer = $("#offiTrailer"),
		vidFrame  = $("#ytplayer");
		
		vidPlayer.slideDown(4000);
		$(vidFrame).attr("src", "http://www.youtube.com/embed/"+trailerId);
			
		/*$.each(medias["media$thumbnail"], function() {
			$('<img src="'+this.url+'" />').appendTo($('#list'));
		});*/
	},
	
	playTrailer: function (trailerId) {
		var that = this;
		that.vidPlayer.slideDown(1000);
		$(that.vidFrame).prop("src", that.vidEmbedUrl+trailerId);
	}
	
	
});

//var index = new TrailerApp.Views.Index();
