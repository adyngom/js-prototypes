(function (Backbone, TrailerApp, $) { 
	TrailerApp.Router = Backbone.Router.extend({
	
	initialize: function(el) {
		log("[router initialize()]");
		log(this);
		this.el = el;
		
		this.currentCat = TrailerApp.Config.current_cat;
		
		this.indexView = new TrailerApp.Views.ContentView('#movies-list');
		this.catView = new TrailerApp.Views.ContentView('#movies-list');
		this.movieView = new TrailerApp.Views.ContentView('#movie-entry');
	},
	
	routes: {
		""                : "index",
		"cat/"            : "category",
		"cat/:category"   : "category",
		"movie/:movie_id" : "movie",
		"*other"          : "defaultRoute"
	},
	
	currentView: null,

	switchView: function(view) {
		if (this.currentView) {
			// Detach the old view
			this.currentView.remove();
		}

		// Move the view element into the DOM (replacing the old content)
		this.el.html(view.el);

		// Render view after it is in the DOM (styles are applied)
		//view.render();

		this.currentView = view;
	},
	
	switchCat: function (cat) {
		var that = this;
		var catData = "";
		alert("let's check out the : "+cat.split("_").join(" ")+"!!");
		if(!!TrailerApp.Datasets[cat]) {
			alert("we found a match");
			var set = TrailerApp.Datasets[cat];
			log(TrailerApp.Datasets.cat);
			TrailerApp.trailers.reset(TrailerApp.Datasets.cat);
			that.resetIndex(TrailerApp.trailers);
		}
		else {
			var config = TrailerApp.Config,
				params = {
					url      : config.api+config[cat]+"?apikey="+config.key+"&limit="+config.limit,
					dataType : "jsonp",
					success  : function (data) {
									log("[success]");
									log(that);
									that.refreshTrailers (data, cat);
					},
					error    : that.showError
				}
				
			$.ajax(params);
		}
		//return catData;
	},
	
	refreshTrailers: function (data, cat) {
		var that = this;
		
		log(that);
		log("[refreshTrailers]");
		log(data);
		
		that.currentCat = cat;
		TrailerApp.Datasets[cat] = data.movies;
		TrailerApp.trailers.reset(data.movies);
		log(TrailerApp.trailers);
		
		that.switchView(that.catView);
		
		that.resetIndex(TrailerApp.trailers);
	},
	
	index : function () {
		//log(TrailerApp.trailers);
		var that = this;
		that.switchView(that.indexView);
		if (!!TrailerApp.Datasets["box_office"]) {
			TrailerApp.trailers.reset(TrailerApp.Datasets["box_office"]);
		}
		
		that.resetIndex(TrailerApp.trailers);
		//index.show();
	},
	
	resetIndex: function (newCollection) {
		var index = new TrailerApp.Views.Index({
			collection : TrailerApp.trailers,
			back       : true
		});
	},
	
	category : function (resource) {
		var that = this;
		if(!!resource && resource !== that.currentCat) {
			log(resource);
			log(that.el);
			/*$(that.el).fadeOut(4000, function(event) {
				$(this).fadeIn(5000);
			});*/
			switch (resource) {
				case 'box_office':
				case 'in_theaters':
				case 'opening_movies':
				case 'upcoming_movies':
				case 'top_rentals_dvds':
				case 'current_release_dvds':
				case 'new_release_dvds':
				case 'upcoming_dvds':
					that.switchCat(resource);
					break;
				default:
					return false;
					break;	
			}
		}	
	},
	
	movie : function (movie_id) {
		log("[movie_id]");
		log(movie_id);
		this.switchView(this.movieView);
		var movie      = new TrailerApp.Models.Trailer(TrailerApp.trailers.models[movie_id]);
		var movie_view = new TrailerApp.Views.Movie({
			model: movie,
			back : true
		});
		
		//movie_view.show();
		
	},
	
	defaultRoute : function (other) {
		log("401 Not Found. The resource "+other+" does not exist in this app");
	}
});
})(Backbone, TrailerApp, jQuery);