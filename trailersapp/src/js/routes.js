(function (Backbone, TrailerApp, $) { 
	TrailerApp.Router = Backbone.Router.extend({
	
	initialize: function(el) {
		log("[router initialize()]");
		log(this);
		this.el = el;
		
		this.defaultCat = TrailerApp.Config.default_cat;
		
		this.indexView = new TrailerApp.Views.ContentView('#movies-list');
		this.catView = new TrailerApp.Views.ContentView('#movies-list');
		this.movieView = new TrailerApp.Views.ContentView('#movie-entry');
	},
	
	routes: {
		""                : "index",
		"cat/"            : "category",
		"cat/:category"   : "category",
		"movie/:movie_id" : "movie",
		"*other"          : "defaultRoute"
	},
	
	currentView: null,

	switchView: function(view) {
		if (this.currentView) {
			// Detach the old view
			this.currentView.remove();
		}

		// Move the view element into the DOM (replacing the old content)
		this.el.html(view.el);

		// Render view after it is in the DOM (styles are applied)
		//view.render();

		this.currentView = view;
	},
	
	switchCat: function (cat) {
		var that = this;
		var catData = "";
		//alert("let's check out the : "+cat.split("_").join(" ")+"!!");
		if(!!TrailerApp.Datasets[cat]) {
			TrailerApp.Collections.trailers.remove(TrailerApp.Dataset[that.defaultCat]);
			TrailerApp.Collections.trailers.add(TrailerApp.Dataset[cat]);
		}
		else {
			var config = TrailerApp.Config,
				params = {
					url      : config.api+config[cat]+"?apikey="+config.key+"&limit="+config.limit,
					dataType : "jsonp",
					success  : function (data) {
									log("[success]");
									log(that);
									that.refreshTrailers (data, that);
					},
					error    : that.showError
				}
				
			$.ajax(params);
		}
		//return catData;
	},
	
	refreshTrailers: function (data, that) {
		log(that);
		log("[refreshTrailers]");
		log(data);
		TrailerApp.trailers.reset(data.movies);
		log(TrailerApp.trailers);
		
		that.switchView(that.catView);
		var category = new TrailerApp.Views.Index({
			collection : TrailerApp.trailers,
			back       : true
		});
		
		category.render();
	},
	
	index : function () {
		//log(TrailerApp.trailers);
		var that = this;
		that.switchView(that.indexView);
		var index = new TrailerApp.Views.Index({
			collection : TrailerApp.trailers,
			back       : false
		});
		
		//index.show();
	},
	
	category : function (resource) {
		var that = this;
		if(!!resource) {
			log(resource);
			log(that.el);
			$(that.el).fadeOut(4000, function(event) {
				$(this).fadeIn(5000);
			});
			switch (resource) {
				case 'box_office':
				case 'in_theaters':
				case 'opening_movies':
				case 'upcoming_movies':
				case 'top_rentals':
				case 'current_releases':
				case 'new_releases':
				case 'upcoming':
					that.switchCat(resource);
					break;
				default:
					return false;
					break;	
			}
		}	
	},
	
	movie : function (movie_id) {
		log("[movie_id]");
		log(movie_id);
		log(TrailerApp.trailers.getByCid("c"+movie_id));
		this.switchView(this.movieView);
		var movie      = new TrailerApp.Models.Trailer(TrailerApp.trailers.getByCid("c"+movie_id));
		var movie_view = new TrailerApp.Views.Movie({
			model: movie,
			back : true
		});
		
		//movie_view.show();
		
	},
	
	defaultRoute : function (other) {
		log("401 Not Found. The resource "+other+" does not exist in this app");
	}
});
})(Backbone, TrailerApp, jQuery);
