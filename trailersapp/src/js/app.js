var TrailerApp = (function (Backbone, $) {  
	return {
		Config      : {
			key                  : "g9s9gyu8qd8f4q6fzvwjmgpf",
			api                  : "http://api.rottentomatoes.com/api/public/v1.0/lists/",
			limit                : 50,
			default_cat          : "opening_movies",
			current_cat          : "opening_movies",
			box_office           : "movies/box_office.json",
			in_theaters          : "movies/in_theaters.json",
			opening_movies       : "movies/opening.json",
			upcoming_movies      : "movies/upcoming.json",
			// dvds
			top_rentals          : "dvds/top_rentals.json",
			current_releases     : "dvds/current_releases.json",
			new_releases         : "dvds/new_releases.json",
			upcoming             : "dvds/upcoming.json"
			
		},
		Datasets    : {},
		Models      : {},
		Collections : {},
		Views       : {},
		Events      : {},
		Utils       : {},
		
		init: function () {
			
			var that   = this,
				config = that.Config;
				log("[App Init]");
				log(that);
				log(config);
			$.ajax({
				url      : config.api+config.opening_movies+"?apikey="+config.key+"&limit="+config.limit,
				dataType : "jsonp",
				success  : that.showIndex,
				error    : that.showError
			});
		},
		
		showIndex: function (data) {
			log("[showIndex]");
			log(data);
			TrailerApp.Datasets["box_office"] = data.movies;
			TrailerApp.trailers = new TrailerApp.Collections.Trailers(data.movies);
			
			TrailerApp.trailers.on('reset', function (collection, options) {
			    this.each(function (model, index, list) {
			        // do something here with index
			        //console.log(model, index);
			       
			    });
			});
			
			TrailerApp.router = new TrailerApp.Router($("#main"));
			Backbone.history.start();
		},
		
		showError: function (jqXHR, textStatus, errorThrown) {
			log(jqXHR);
			log(textStatus);
			log(errorThrown);
			//log(jqXHR.responseText);
		}
	};
})(Backbone, jQuery);


