;(function(){
    var node = typeof window === "undefined",
        glb = node ? global : window,
        TIMEOUT = 1,
        INTERVAL = 2,
        timers = [],
        Soot = function(conf){
            var self = this;
            
            if(!conf.cb){
                throw "A callback is required.";
            }
            
            if(!conf.time){
                throw "A time is required.";
            }
            
            if(!(conf.type === INTERVAL || conf.type == TIMEOUT)){
                conf.type = TIMEOUT;
            }
            
            this._ = {
                type : conf.type,
                cb : conf.cb,
                time : conf.time,
                params : conf.params || [],
                context : conf.context || glb,
                noDelay : Boolean(conf.noDelay)//,
                //iterations : conf.iterations || Infinity
            };
            
            if(this._.type === TIMEOUT){
                this._.timer = function(){
                    return timeout(self._.cb, self._.time, self._.params, self._.context);
                };
            }else{
                this._.timer = function(){
                    return interval(self._.cb, self._.time, self._.params, self._.context, self._.noDelay/*, self._.iterations + 1*/);
                };
            }
            
            if(!conf.defer){
                this.start();
            }
        },
        timeout = function(cb, time, args, context){
            var timer = (function(){
                    var id = timers.length;
                    
                    glb.setTimeout(function(args){
                        if(timers[id]){
                            cb.apply(context, args);
                            
                            timers[id] = false;
                        }
                    }, time, args);
                    
                    return id;
                })();
            
            timers[timer] = true;
            
            return timer;
        },
        interval = function(cb, time, args, context, noDelay){
            var timer = (function(){
                    var id = timers.length;
                    
                    if(noDelay){
                        cb.apply(context, args);
                    }
                    
                    glb.setTimeout(function callee(args){
                        if(timers[id]){
                            cb.apply(context, args);
                            
                            glb.setTimeout(callee, time, args);
                        }
                    }, time, args);
                    
                    return id;
                })();
            
            timers[timer] = true;
            
            return timer;
        },
        clear = function(id){
            timers[id] = false;
        };
    
    Soot.prototype = {
        _ : {},
        id : undefined,
        start : function(){
            if(this.active()){
                throw "Timer is already active.";
            }
            
            this.id = this._.timer();
            
            timers[this.id] = true;
            
            return this;
        },
        stop : function(){
            clear(this.id);
            
            timers[this.id] = false;
            
            return this;
        },
        active : function(){
            return !!timers[this.id];
        },
        asInterval : function(noDelay){
            this.stop();
            
            return new this({
                type : this.INTERVAL,
                cb : this._.cb,
                time : this._.time,
                params : this._.params,
                context : this._.context,
                noDelay : noDelay,
                defer : true,
            });
        },
        asTimeout : function(){
            this.stop();
            
            return new this({
                type : this.TIMEOUT,
                cb : this._.cb,
                time : this._.time,
                params : this._.params,
                context : this._.context,
                defer : true
            });
        }
    };
    
    Soot.TIMEOUT = TIMEOUT;
    Soot.INTERVAL = INTERVAL;
    
    Soot.setTimeout = function(cb, time, params, context){
        return new this({
            type : this.TIMEOUT,
            cb : cb,
            time : time,
            params : params,
            context : context
        });
    };
    
    Soot.createTimeout = function(cb, time, params, context){
        return new this({
            type : this.TIMEOUT,
            cb : cb,
            time : time,
            params : params,
            context : context,
            defer : true
        });
    };
    
    Soot.setInterval = function(cb, time, params, noDelay, context){
        return new this({
            type : this.INTERVAL,
            cb : cb,
            time : time,
            params : params,
            context : context,
            noDelay : noDelay
        });
    };
    
    Soot.createInterval = function(cb, time, params, noDelay, context){
        return new this({
            type : this.INTERVAL,
            cb : cb,
            time : time,
            params : params,
            context : context,
            noDelay : noDelay,
            defer : true
        });
    };
    
    if(node){
        module.exports = Soot;
    }else{
        glb.Soot = Soot;
    }
})();
