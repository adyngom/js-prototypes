var soot = require('./soot');

timer1 = soot.setTimeout(function(){
    args = [].slice.call(arguments);
    
    console.log(args.join(' '));
}, 2000, ['hello', 'world']);

timer2 = soot.setTimeout(function(){
    console.log('blah');
}, 4000);

timer3 = soot.createTimeout(function(){
    console.log('hi');
}, 5000);

timer4 = soot.setTimeout(function(){
    timer3.start();
}, 1000);

timer2.stop();

interval1 = soot.setInterval(function(){
    console.log('interval');
}, 1000);

timer5 = soot.setTimeout(function(){
    interval1.stop();
}, 5000);
